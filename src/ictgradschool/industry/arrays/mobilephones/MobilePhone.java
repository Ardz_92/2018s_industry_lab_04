package ictgradschool.industry.arrays.mobilephones;

public class MobilePhone {

    // TODO Declare the 3 instance variables:
    private String brand;
    private String model;
    private double price;
    
    public MobilePhone(String brand, String model, double price) {
        // Complete this constructor method
        this.brand = brand;
        this.model = model;
        this.price = price;
    }

    // TODO Uncomment these methods once the corresponding instance variable has been declared.
    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }
    
    // TODO Insert getModel() method here
    public String getModel() {
            return model;
        }
    
    // TODO Insert setModel() method here
    public void setModel(String brand) {
        this.model = model;
    }

    // TODO Insert getPrice() method here
    public double getPrice() {
        return price;
    }
    
    // TODO Insert setPrice() method here
    public void setPrice(double price) {
        this.price = price;
    }

    // TODO Insert toString() method here
    public String toString(){
        String line = "";
        return brand + " " + model + " " + "$" + price;
    }
    
    // TODO Insert isCheaperThan() method here

    public boolean isCheaperThan(MobilePhone other) {
        if ( this.price > other.price) {
            return true;
        }
        return false;
    }

    // TODO Insert equals() method here
    @Override
    public boolean equals(Object other) {
        if (other instanceof MobilePhone){
            MobilePhone otherp = (MobilePhone) other;
            return this.price == otherp.price && this.model.equals(otherp.model)
                    && this.brand.equals(otherp.brand);
        }
        return false;
    }
}


