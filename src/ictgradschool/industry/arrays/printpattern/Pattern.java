package ictgradschool.industry.arrays.printpattern;

public class Pattern {
    public char patternSymbol;
    public int patternLength;

    public Pattern(int patternLength, char patternSymbol) {
        this.patternSymbol = patternSymbol;
        this.patternLength = patternLength;
    }

    public char getPatternSymbol() {
        return patternSymbol;
    }

    public void setPatternSymbol(char patternSymbol) {
        this.patternSymbol = patternSymbol;
    }

    public int getPatternLength() {
        return patternLength;
    }

    public void setPatternLength(int patternLength) {
        this.patternLength = patternLength;
    }

    @Override
    public String toString() {
        String line = "";

        for (int i = 0; i < patternLength; i++ ){
            line += patternSymbol;
        }
        return line;
    }

}
